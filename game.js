
var winningNumber; //randomly generated number
var min; //used for generating the random number
var max; //used for generating the random number
var response; //number guessed by the user
var gameLevel = localStorage.getItem("level");
var maxWinNumber;
var maxTurns;
var turns;

if (gameLevel === "beginner") {
  turns = 7; //sets the number of turns left to 7 - maximum number of tries allowed
  //creates a random number inclusive of minimum and maximum
  winningNumber = getRandomInt(1, 100);
  maxWinNumber = 100; 
  maxTurns = 7;
  
}

if (gameLevel === "intermediate") {
  turns = 10; //sets the number of turns left to 10 - maximum number of tries allowed
  //creates a random number inclusive of minimum and maximum
  winningNumber = getRandomInt(1, 500);
  maxWinNumber = 500;
  maxTurns = 10;
  
}

if (gameLevel === "advanced") {
  turns = 20; //sets the number of turns left to 20 - maximum number of tries allowed
  //creates a random number inclusive of minimum and maximum
  winningNumber = getRandomInt(1, 1000);
  maxWinNumber = 1000;
  maxTurns = 20;
 
}

var instructionText = "To play, just enter a number <br> between 1 and " + maxWinNumber + ".<br><br>You get " + maxTurns + " guesses.<br> I will try to help you along the way.<br> I know you can do it!! <br>Let's play!";
document.getElementById("turnsLeft").innerHTML = "&nbsp" + maxTurns;
document.getElementById("instructions").innerHTML = instructionText;
document.getElementById("guessNumInput").focus(); 


//hides the "Try Again" button so that it isn't available until the game is over
document.getElementById("tryAgain").style.display = "none";


document.addEventListener("keydown", function(event) {
  console.log(event.keyCode);
  if (event.keyCode == 13) {
    runProgram();
  } else {
    return;
  }
})


//function is called when Submit button is pushed
function runProgram() {
  
  if (turns > 0) { //only runs if there are turns left
    
    //gets the number that the user entered
    var char = document.getElementById("guessNumInput").value;
    response = Number(char);
  
    //in case of invalid response
    if (response < 1 || response > maxWinNumber || response === NaN) { 
      window.alert("The response you entered was not between 1 and " + maxWinNumber + ".");
      document.getElementById("guessNumInput").value=''; //clears number field
      return; //immediately exits the function and gives control back to the user
    }
    
    //if response is valid
    if (response === winningNumber) { //if user has guessed the winning number
      turns--; //counts the winning turn as a turn
      if (turns !== maxTurns - 1) { //formats response correctly if user won in multiple turns
         document.getElementById("feedback").innerHTML += "You won in " + (maxTurns - turns) + " turns!";
      }
      if (turns === maxTurns - 1) { //formats reponse correctly if user won in one turn
         document.getElementById("feedback").innerHTML += "You won in 1 turn!";
      }
      //sets display of number of turns left
      document.getElementById("turnsLeft").innerHTML = "&nbsp" + turns;
       
      youWin(); //runs the youWin function
        
      turns = 0; //sets internal count of turns to 0, so program is unresponsive until game is reset
      return; //immediately ends program
      
    } else { //if haven't won yet
      
        if (response < winningNumber) { //if winning number is greater than user guess
            document.getElementById("feedback").innerHTML += "Greater than " + response + "<br>";
        } else { //if winning number is less than user guess
                document.getElementById("feedback").innerHTML += "Less than " + response + "<br>";
        } // end of if less than or greater than
      
    } //end of if winning or not winning       
   
    turns--;
    if (turns === 0) { //if user runs out of turns and loses
      document.getElementById("feedback").innerHTML += "The winning number was " + winningNumber + ".";
      youLose(); //runs youLose function
    } //end of if out of turns
    
    document.getElementById("turnsLeft").innerHTML = "&nbsp" + turns; //updates number of turns left
    document.getElementById("guessNumInput").value=''; //erases number field
    document.getElementById("guessNumInput").focus(); 
    
  } //end of if turns greater than zero
   
} //end of runProgram function

//creates a random number inclusive of maximum and minimum
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; 
}

//reloads screen so that all number reset to play again
//this function doesn't work in codePen unless you are in Debug mode
function resetProgram() { 
  location.reload(true);
  window.document.location="main_game.html"
  return false;
}

//runs when user runs out of turns
function youLose() {
  document.getElementById("tryAgain").style.display = "block"; //displays "Try Again" button
  //displays "You Lose" graphic in popup window
  window.open("https://i.ibb.co/JjT02RH/you-lose-two.jpg", "_blank", "toolbar=no,scrollbars=no,resizable=no,top=100,left=300,width=700,height=400");
}
  
//runs when user wins
function youWin() {
  document.getElementById("tryAgain").style.display = "block"; //displays "Try Again" button
  //displays "You Win" graphic in popup window
  window.open("https://i.ibb.co/cQh2Vp3/you-won-two.jpg", "_blank", "toolbar=no,scrollbars=no,resizable=no,top=100,left=300,width=700,height=400");
}


