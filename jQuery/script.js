//Per defined array of quotes to use
var quotes = ["There will be a day when I no longer can do this. THAT DAY IS NOT TODAY.","Your friends should motivate and inspire you. Your circle should be well rounded and supportive. Keep it tight. Quality over quantity, always.","Don’t make change too complicated. Just begin.","YOU are the creator of your own destiny.","This is the start of something beautiful.","Don’t let small minds convince you that your dreams are too big.","The temptation to quit will be greatest just before you are about to succeed.","Never give up on a dream just because of the time it will take to accomplish it. The time will pass anyway.","Ask yourself if what you’re doing today is getting you closer to where you want to be tomorrow.","The greatest pleasure in life is doing what people say you cannot do."];

//keeps track of the index of each quote that has already been used, to stop duplicates
var quotesUsed =[];

$("button").click(function() {
  createQuote();
});

//randomly picks a quote from above array, creates a new div and adds the quote text and displays it on screen using the "slideDown" method. Adds the click method to div so that it can be erased when clicked.
function createQuote() {
  var divCreate = document.createElement("div");
  var random;

  if (quotesUsed.length !== quotes.length) {
      do {
          random = (Math.floor((Math.random() * (quotes.length)) + 1))-1;
      }
      while (quotesUsed.includes(random));
  
      quotesUsed.push(random);
  
      $(divCreate).hide();
      $("body").append(divCreate);
      $(divCreate).text(quotes[random]);
      $(divCreate).slideDown( "slow", function() {});

      $(divCreate).click(function() {
          deleteQuote($(this),quotesUsed);
      });
  } else {
    alert ("Sorry, we are out of quotes.");
  }
}

//called when div is clicked on for deletion
//removes div with fadeOut method and remove
//Also removes the index of that quote from the quotesUsed array so that it can be readded
function deleteQuote(thisObj) {
    thisObj.fadeOut(300, function(){
        thisObj.remove();  
    }); 
    var quoteIndex = quotes.indexOf(thisObj.text());
    quotesUsed.splice(quotesUsed.indexOf(quoteIndex), 1);
}
